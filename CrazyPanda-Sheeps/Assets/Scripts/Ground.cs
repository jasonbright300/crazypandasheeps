﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
    public Collider Collider;

    public Vector3 Extents => Collider.bounds.extents;
}
