﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using Assets.Scripts.Utility;
using UnityEngine;
using Zenject;

public class SheepAI : MonoBehaviour, IController
{
    public Vector2 Direction { get; private set; }

    private Vector3 _currentTarget;

    [Inject] private Ground _ground;
    [Inject] private Platform _platform;

    void Start()
    {
        _currentTarget = GetRandomTarget();
    }

    void Update()
    {
        var currentPos = transform.position;
        currentPos.y = 0;

        if (Vector3.Distance(currentPos, _currentTarget) < 1.0F)
        {
            _currentTarget = GetRandomTarget();
        }

        var inverse = transform.InverseTransformPoint( _currentTarget ).normalized;
        var xModule = Mathf.Abs( inverse.x );
        var xDirection = (xModule < 0.5F ? 0.5F : xModule) * Mathf.Sign( inverse.x );
        Direction = new Vector2(xDirection, inverse.z);
    }

    Vector3 GetRandomTarget()
    {
        switch (_platform.CurrentState)
        {
            case Platform.State.Activated:
            case Platform.State.Highlighted:
                return _platform.Collider.GetRandomPointInsideCollider( 0.8F );
            case Platform.State.Hided:
                return _ground.Collider.GetRandomPointInsideCollider( 0.8F );
        }
        return Vector3.zero;
    }
}
