﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using DG.Tweening;
using UnityEngine;
using Zenject;

public class Sheep : MonoBehaviour
{
    public Vector2 Direction => _controller.Direction;

    public Rigidbody Rigidbody;
    public Animator Animator;

    [Inject(Id = "Speed")]
    public float Speed;

    [Inject(Id = "AngularSpeed")]
    public float AngularSpeed;

    [Inject(Id = "Force")]
    public float Force;

    [Inject]
    private IController _controller;

    private bool _freezed = false;

    void FixedUpdate()
    {
        if (_freezed)
            return;

        var forwardSpeed = Mathf.Max( Mathf.Abs( Direction.y ), Mathf.Abs( Direction.x ) ) * Speed;
        var forwardDirection = forwardSpeed /** Mathf.Sign( Direction.y )*/;

        var nextPosition = Rigidbody.position + transform.forward * forwardDirection * Time.fixedDeltaTime;
        Rigidbody.MovePosition( nextPosition );

        var euler = transform.eulerAngles;
        euler.y += Direction.x * AngularSpeed * Time.fixedDeltaTime;
        transform.eulerAngles = euler;

        Animator.SetFloat( "MoveSpeed", forwardSpeed );
    }

    void OnCollisionEnter( Collision collision )
    {
        if (_freezed)
            return;
        if (collision.transform.tag != "Sheep")
            return;

        var invers = transform.InverseTransformPoint( collision.transform.position ).normalized;
        if (invers.z > 0)
        {
            var vector = collision.transform.position - transform.position;
            vector.y = 0;
            collision.transform.GetComponent<Sheep>().Rigidbody.AddForce( vector * Force, ForceMode.Impulse);
            _freezed = true;
            StartCoroutine( DoUnfreeze() );
        }
    }

    IEnumerator DoUnfreeze()
    {
        yield return new WaitForSeconds( 0.3F );
        _freezed = false;
    }

    public class SheepFactory : PlaceholderFactory<Sheep>
    {

    }
}