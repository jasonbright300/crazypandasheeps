﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class AiInstaller : MonoInstaller
{
    public float AngularSpeed;

    public override void InstallBindings()
    {
        Container.Bind<float>().WithId( "AngularSpeed" ).FromInstance( AngularSpeed );
    }
}
