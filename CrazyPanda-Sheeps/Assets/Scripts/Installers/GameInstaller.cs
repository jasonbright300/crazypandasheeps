using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    public Sheep AISheepPrefab;
    public GameObject KillEffectPrefab;

    public override void InstallBindings()
    {
        Container.Bind<SheepsSpawner>().AsSingle().NonLazy();
        Container.BindFactory<Sheep, Sheep.SheepFactory>().FromComponentInNewPrefab( AISheepPrefab );

        Container.BindInterfacesTo<SheepKiller>().AsSingle().WithArguments( KillEffectPrefab ).NonLazy();
    }
}