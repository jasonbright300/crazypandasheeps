﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

public class SheepKiller : ITickable
{
    private SheepsSpawner _sheepsSpawner;
    private Platform _platform;
    private GameObject _effectPrefab;

    public SheepKiller( SheepsSpawner spawner, Platform platform, GameObject effectPrefab )
    {
        _sheepsSpawner = spawner;
        _platform = platform;
        _effectPrefab = effectPrefab;
    }

    public void Tick()
    {
        for(int i = _sheepsSpawner.Sheeps.Count - 1; i >= 0; i--)
        {
            var sheep = _sheepsSpawner.Sheeps[i];
            var posY = sheep.transform.position.y;

            if (posY < -1)
            {
                Kill(sheep);
                continue;
            }

            if (_platform.CurrentState == Platform.State.Activated && posY < _platform.Collider.bounds.extents.y)
            {
                Kill(sheep);
            }
        }
    }

    void Kill( Sheep sheep )
    {
        _sheepsSpawner.Sheeps.Remove( sheep );
        sheep.StartCoroutine( DoKill( sheep ) );
    }

    IEnumerator DoKill(Sheep sheep)
    {
        yield return new WaitForSeconds( 0.5F );
        GameObject.Instantiate( _effectPrefab, sheep.transform.position, Quaternion.identity );
        yield return new WaitForSeconds( 0.3F );
        GameObject.Destroy( sheep.gameObject );
    }
}