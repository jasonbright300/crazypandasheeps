﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class CameraController : MonoBehaviour
{
    [Inject] private Sheep PlayerSheep;

    private GameObject target => PlayerSheep.gameObject;

    public float damping = 1;

    public Vector3 offset;

    void Start() {
        offset = target.transform.position - transform.position;
        offset.x = 0;
    }
     
    void LateUpdate()
    {
        if (PlayerSheep == null)
            return;

        float currentAngle = transform.eulerAngles.y;
        float desiredAngle = target.transform.eulerAngles.y;
        float angle = Mathf.LerpAngle(currentAngle, desiredAngle, Time.deltaTime * damping);
         
        Quaternion rotation = Quaternion.Euler(0, angle, 0);
        transform.position = target.transform.position - (rotation * offset);
         
        transform.LookAt(target.transform);
    }
}
