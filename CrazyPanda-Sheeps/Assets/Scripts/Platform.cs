﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Zenject;

public class Platform : MonoBehaviour
{
    public Collider Collider;

    public float HidedPosition = -3.3F;
    public float HighlightedPosition = -2.3F;
    public float ShowedPosition = -0.15F;

    public float Delay = 10.0F;
    public float HighlightedTime = 5.0F;
    public float ActivatedTime = 10.0F;

    public int[] Levels;

    public enum State
    {
        Hided,
        Highlighted,
        Activated
    }

    public State CurrentState { get; private set; }

    [Inject] private Ground _ground;

    void Start()
    {
        StartCoroutine( DoPlatform() );
    }

    IEnumerator DoPlatform()
    {
        for(int i = 0; i < Levels.Length; i++)
        {
            CurrentState = State.Hided;
            var currentLevelSize = Levels[i];
            transform.localScale = new Vector3(currentLevelSize, transform.localScale.y, currentLevelSize);

            //yield return null;
            var pos = GetRandomPosition();
            pos.y = HidedPosition;

            //скрытое состояние платформы
            transform.position = pos;

            yield return new WaitForSeconds( HighlightedTime );
            //подсветка платформы

            CurrentState = State.Highlighted;
            transform.DOMoveY( HighlightedPosition, 0.5F );

            
            yield return new WaitForSeconds( Delay - HighlightedTime );
            transform.DOMoveY( ShowedPosition, 1.0F ).OnComplete( () => CurrentState = State.Activated );

            yield return new WaitForSeconds( ActivatedTime );
            CurrentState = State.Hided;
            const float hideAnimationTime = 1.0F;
            transform.DOMoveY( HidedPosition, hideAnimationTime );

            yield return new WaitForSeconds( hideAnimationTime );
        }
    }

    public Vector3 GetRandomPosition()
    {
        var extents = new Vector3( _ground.Extents.x - Collider.bounds.extents.x, 0,
            _ground.Extents.z - Collider.bounds.extents.z );

        var randPoint = new Vector3(
            Random.Range( -extents.x, extents.x ),
            0,
            Random.Range( -extents.z, extents.z )
        );
        return randPoint;
    }
}