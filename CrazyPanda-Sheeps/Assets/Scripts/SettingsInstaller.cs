using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "SettingsInstaller", menuName = "Installers/SettingsInstaller")]
public class SettingsInstaller : ScriptableObjectInstaller<SettingsInstaller>
{
    public float Force;
    public float Speed;
    public float AngularSpeed;
    public int AiCount;

    public override void InstallBindings()
    {
        Container.Bind<float>().WithId( "Force" ).FromInstance( Force ).IfNotBound();
        Container.Bind<float>().WithId( "Speed" ).FromInstance( Speed ).IfNotBound();
        Container.Bind<float>().WithId( "AngularSpeed" ).FromInstance( AngularSpeed ).IfNotBound();

        Container.Bind<int>().WithId( "SheepsCount" ).FromInstance( AiCount ).IfNotBound();
    }
}