﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Utility
{
    public static class ColliderExtensions
    {
        public static Vector3 GetRandomPointInsideCollider(this Collider collider, float boundsMultiplier)
        {
            var extents = collider.bounds.extents * boundsMultiplier;

            var randPoint = new Vector3(
                Random.Range( -extents.x, extents.x ),
                0,
                Random.Range( -extents.z, extents.z )
            );
            var pos = collider.transform.position + randPoint;
            pos.y = 0;
            return pos;
        }
    }
}
