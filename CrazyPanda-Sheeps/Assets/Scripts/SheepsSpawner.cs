﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Utility;
using Zenject;

public class SheepsSpawner
{
    private Ground _ground;
    private Sheep.SheepFactory _factory;

    public List<Sheep> Sheeps = new List<Sheep>();

    public SheepsSpawner( Sheep.SheepFactory factory, Ground ground, Sheep playerSheep, [Inject(Id = "SheepsCount")] int aiCount )
    {
        _ground = ground;
        _factory = factory;

        Sheeps.Add( playerSheep );

        for (int i = 0; i < aiCount; i++)
        {
            var sheep = _factory.Create();
            sheep.transform.position = _ground.Collider.GetRandomPointInsideCollider( 0.7F );
            Sheeps.Add( sheep );
        }
    }
}