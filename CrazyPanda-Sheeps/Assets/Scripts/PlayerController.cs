﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using Zenject;

public class PlayerController : MonoBehaviour, IController
{
    public Vector2 Direction => new Vector2( _joystick.HorizontalInput(), _joystick.VerticalInput() );

    [Inject] private JoystickMovement _joystick;
}
